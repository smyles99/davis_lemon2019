﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float dashSpeed;
    private int health;//total health
    bool invincible = false;
    public GameEnding gameEnding;
    float dashTimer;
    public GameObject Trail;

    public Text healthText;

    public Renderer myRenderer;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        //Trail.SetActive(false);
        health = 5;
        SetHealthText();
        

        myRenderer = GetComponentInChildren<Renderer>();
    }

    void FixedUpdate()
    {
        
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        HandleDash();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput; //If there is horizontal OR vertical movement isWalking = true
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        if (dashTimer > 0f)
        {
            
            float dashDistance = 1f;
            dashTimer -= Time.deltaTime;
            m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * dashDistance * Time.deltaTime * 5f);
            if (dashTimer < 0f)
            {
                Trail.SetActive(false);
            }
        }
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void HandleDash()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            //float dashDistance = 1f;
            //m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * dashDistance);
            Trail.SetActive(true);
            dashTimer = .2f;
            //transform.position += m_Movement * dashDistance;
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cloak"))
        {
            //Renderer myRenderer = GetComponentInChildren<Renderer>();//Mesh is on the child of JohnLemon so i Have to find that
            myRenderer.enabled = false;
            Invoke("unCloak", 5f);//call after 5 seconds
            //gameEnding.CaughtPlayer() != true;
            invincible = true;

            Destroy(other.gameObject);
            
        }
    }
    void unCloak()
    {
        //Renderer myRenderer = GetComponentInChildren<Renderer>();
        myRenderer.enabled = true;
        invincible = false;
    }
    public void TakeDamage(int damage)
    {
        if (health > 0 && !invincible)
        {
            invincible = true;
            Invoke("EndInvincible", .5f);
            health = health - 1;
            SetHealthText();

            if (health <= 0)
            {
                gameEnding.CaughtPlayer();
            }
        }
    }
    void EndInvincible()
    {
        invincible = false;
    }

    void SetHealthText()
    {
        healthText.text = "Health: " + health.ToString();
    }

}