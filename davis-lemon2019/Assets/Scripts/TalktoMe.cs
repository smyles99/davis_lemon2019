﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TalktoMe : MonoBehaviour
{
    public Text textBox;
    public string toSay;

    private void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("Player"))
        {
            textBox.gameObject.SetActive(true);
            textBox.text = toSay;
        }
    }

    private void OnTriggerExit (Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textBox.gameObject.SetActive(false);
        }
    }
}
